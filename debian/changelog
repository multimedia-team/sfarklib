sfarklib (2.24-5) unstable; urgency=medium

  * Team upload.

  [ Jenkins ]
  * Update watch file format version to 4.
  * Bump debhelper from old 11 to 13.
  * Update pattern for GitHub archive URLs from /<org>/<repo>/tags page/<org>/<repo>/archive/<tag> -> /<org>/<repo>/archive/refs/tags/<tag>.
  * Remove obsolete field Name from debian/upstream/metadata (already present in machine-readable debian/copyright).
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

  [ Sam Q ]
  * Fix watchfile error for searching upstream

 -- Ross Gammon <rossgammon@debian.org>  Sun, 11 Sep 2022 19:12:36 +0200

sfarklib (2.24-4) unstable; urgency=medium

  [ Thorsten Glaser ]
  * Team upload.
  * Probably make this work on big endian platforms
  * Fix decompressed data on any-i386
  * Note that LP64 architectures still suffer from extraction issues:
    https://github.com/raboof/sfArkLib/issues/12 contains a patch but
    that would require an ABI bump; probably worth it, though…
  * debian/source/local-options: auto-unapply patches for git

  [ Ruben Undheim ]
  * Added gitlab-ci.yml to test
  * whitespace fix

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

 -- Thorsten Glaser <tg@mirbsd.de>  Wed, 02 Sep 2020 00:25:23 +0200

sfarklib (2.24-3) unstable; urgency=medium

  * control: New standards version 4.2.1 - no changes
  * debian/tests: Added autopkgtest
  * Delete debian/source/local-options. Does not work with dgit

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 02 Dec 2018 11:08:12 +0100

sfarklib (2.24-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * debian/control:
    - Deprecate priority extra
    - Fix email address for maintainer
    - Set Vcs-* to salsa.debian.org
  * debian/copyright:
    - Use https protocol in Format field
  * Remove some trailing whitespaces

  [ Ruben Undheim ]
  * debian/compat: compat level upgraded to 11
  * debian/control:
    - Add "Mutli-Arch: same" to library
    - Drop libsfark0-dbg since it is handled automatically now
    - debhelper >= 11
    - Standards version to 4.1.5
  * debian/rules:
    - Create install dirs in override_dh_auto_install
    - Enable hardening=+all
    - Remove manual handling of dh_strip
    - Remove target "get-orig-source"

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 30 Jul 2018 19:55:02 +0200

sfarklib (2.24-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches
  * Unapply patches from source
  * Improve gbp.conf so that tags are always signed & branch layout described
  * Add upstream/metadata
  * Patches forwarded upstream

 -- Ross Gammon <rossgammon@mail.dk>  Sun, 13 Sep 2015 14:21:46 +0200

sfarklib (2.23+20131219gitee08d0c-1) unstable; urgency=low

  * Initial release (Closes: #768169)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Wed, 05 Nov 2014 17:53:22 +0100
